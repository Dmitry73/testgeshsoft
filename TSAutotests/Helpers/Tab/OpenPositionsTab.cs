﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;

namespace TSAutotests
{
    class OpenPositionsTab
    {
        private const string TAB_TITLE = "Trades";
        private const string CLOSE_ALL_POSITION_POPUP_MENU = "Close All Positions...";

        private ListView openPositionsView;

        public int OpenPositionsCount
        {
            get
            {   
                return openPositionsView.RowCount;
            }
        }

        public OpenPositionsTab(TradingStationWindow application)
        {
            ApplicationMenuHelper menuHelper = new ApplicationMenuHelper(application);
            menuHelper.PressViewOpenPositions();

            openPositionsView = (ListView)application.FindChild("SysListView32", TAB_TITLE, true);
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    openPositionsView.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
