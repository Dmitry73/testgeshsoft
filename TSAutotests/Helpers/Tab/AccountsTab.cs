﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSAutotests.Helpers.Tab
{
    class AccountsTab
    {
        private const string ACCOUNTS_TAB_TITLE = "Account";

        private ListView accountsView;

        public AccountsTab(TradingStationWindow application)
        {
            TradingStationTab accountsTab = null;
            if (application.FindTab(ACCOUNTS_TAB_TITLE) == null)
            {
                var applicationMenu = new ApplicationMenuHelper(application);
                applicationMenu.PressViewAccounts();
                Waiter.WaitingCondition condition = () => (application.FindTab(ACCOUNTS_TAB_TITLE)) != null;
                if (!Waiter.Wait(condition))
                    throw new Exception("tab Orders isn't found");
            }
            accountsTab = application.FindTab(ACCOUNTS_TAB_TITLE);
            accountsTab.Click();

            accountsView = (ListView)application.FindChild("SysListView32", ACCOUNTS_TAB_TITLE, true);
        }

        public bool CheckTabIsReady()
        {
            return !accountsView.CellValue(0, 9).Equals("");  //HACK no ability to track count of columns
        }
    }
}
