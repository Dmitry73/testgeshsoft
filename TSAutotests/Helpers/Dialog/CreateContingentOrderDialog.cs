﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Collections.Generic;

namespace TSAutotests
{
    class CreateContingentOrderDialog : IDisposable
    {
        private const string ORDER_TYPE = "Order Type:";
        private const string OK_BUTTON = "OK";
        private const int PRIMARY_CURRENCY_PAIR_ID = 2202;
        private const int PRIMARY_SELL_BUY_ID = 2207;
        private const int PRIMARY_RATE_ID = 2204;
        private const int SECONDARY_CURRENCY_PAIR_ID = 2302;
        private const int SECONDARY_SELL_BUY_ID = 2307;
        private const int SECONDARY_RATE_ID = 2304;
        private const string CONTINGENT_ORDER_DIALOG_WINDOW = "Create Contingent Order";
        private const string ORDER_TYPE_OTO = "OTO";

        private Dialog dialog;

        public CreateContingentOrderDialog(TradingStationWindow application)
        {
            dialog = application.GetDialog(CONTINGENT_ORDER_DIALOG_WINDOW);

            if (dialog == null)
            {

                var applicationMenu = new ApplicationMenuHelper(application);
                applicationMenu.PressOpenCreateContingentOrder();

                Waiter.WaitingCondition condition = () => application.GetDialog(CONTINGENT_ORDER_DIALOG_WINDOW) != null;
                if (!Waiter.Wait(condition))
                {
                    throw new Exception("Not found window Create Contingent Order");
                }
                else
                {
                    dialog = application.GetDialog(CONTINGENT_ORDER_DIALOG_WINDOW);
                }
            }
        }


        public void CreateOtoOrder(string primaryCurrencyPair, double exhibitionPrimaryRate,
            string secondaryCurrencyPair, double exhibitionSecondaryRate)
        {
           
            var orderType = (ComboBox)dialog[ORDER_TYPE];

            Button okButton = (Button)dialog[OK_BUTTON];
            ComboBox primaryCurrencyPairComboBox = (ComboBox)dialog[PRIMARY_CURRENCY_PAIR_ID];
            ComboBox primarySellBuy = (ComboBox)dialog[PRIMARY_SELL_BUY_ID];
            EditBox primaryRate = (EditBox)dialog[PRIMARY_RATE_ID];
            ComboBox secondaryCurrencyPairComboBox = (ComboBox)dialog[SECONDARY_CURRENCY_PAIR_ID];
            ComboBox secondarySellBuy = (ComboBox)dialog[SECONDARY_SELL_BUY_ID];
            EditBox secondaryRate = (EditBox)dialog[SECONDARY_RATE_ID];

            primaryCurrencyPairComboBox.SelectItem(primaryCurrencyPair);
            secondaryCurrencyPairComboBox.SelectItem(secondaryCurrencyPair);

            primaryRate.Text = Convert.ToString(exhibitionPrimaryRate);
            secondaryRate.Text = Convert.ToString(exhibitionSecondaryRate);
            orderType.SelectItem(ORDER_TYPE_OTO);
            okButton.Click();

            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition))
                throw new Exception("Dialog Create Contingent Order not close");
        }

        int findOffer(TradingStationSimpleView offers, String instrument)
        {
            for (int i = 0; i < offers.Grid.RowCount; i++)
            {
                if (instrument.Equals(offers.Grid.CellValue(i, 0)))
                    return i;
            }
            return -1;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
