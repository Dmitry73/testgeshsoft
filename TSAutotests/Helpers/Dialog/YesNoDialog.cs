﻿using gehtsoft.applicationcontroller;

namespace TSAutotests
{
    static class YesNoDialog
    {
        public static bool PressYes(string title, Window app)
        {
            using (Dialog dlg = Waiter.WaitDialogAppear(app, title))
            {
                if (dlg == null)
                {
                    return false;
                }
                else
                {
                    dlg["Yes"].Click();
                    return true;
                }
            }
        }
    }
}
