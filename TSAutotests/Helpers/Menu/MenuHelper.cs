﻿using System;
using gehtsoft.applicationcontroller;

namespace TSAutotests
{
    class MenuHelper
    {
        protected static WindowMenuItem findSubMenuItemByName(WindowMenu menu, String name)
        {
            WindowMenuItem result = null;
            foreach (WindowMenuItem item in menu)
            {
                if (item.Text.Equals(name))
                {
                    result = item;
                    break;
                }
            }
            return result;
        }
    }

}
